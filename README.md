# EEland: easy eland

[eland](https://github.com/elastic/eland) helps you to easily retrieve and work on data from elasticsearch.
Thies project provides 2 interfaces to make it easier to use eland in data pipelines: a python cli and a docker image.

Results are sorted by timestamp by default. This may impact performance significantly because it's has to be done on a pandas DataFrame, and can't be done on an eland dataframe

```bash
pip install eeland
# display all available parameters
eeland --help
# query all data from the provided index
eeland --index myindex --host localhost --port 9200 --user elastic --password changeme
# query all data from the provided index and store results into a csv locally
eeland --index myindex --host localhost --port 9200 --user elastic --password changeme --to-csv myindex.csv
```
