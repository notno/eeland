FROM python:3.9-slim

RUN apt update && apt upgrade -y
WORKDIR /project
COPY pyproject.toml .
COPY eeland eeland
RUN pip install .
ENTRYPOINT [ "eeland" ]
CMD ["--help"]
